section .data
numbers: db "0123456789"

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - туда адрес
print_string:
    xor rax, rax
    xor rsi, rsi
.loop:
    lea rsi, [rdi + rax]
    cmp byte[rsi], 0
    je .end
    push rax
    push rdi
    mov dil, byte[rsi]
    call print_char
    pop rdi
    pop rax
    inc rax
    jmp .loop
.end:
    ret



; Принимает код символа и выводит его в stdout из rdi
print_char: 
    mov rax, 1
    push rdi
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    xor rax, rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rsi, 10
    xor r8, r8
    .loop:
        xor rdx, rdx
        div rsi
        push word[numbers+rdx]
        inc r8
        cmp rax, 0
        jnz .loop

    .rloop:
        xor rdi, rdi
        pop di
        call print_char
        sub r8, 1
        cmp r8, 0
        jnz .rloop

    xor rax, rax
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int: 
    mov r8, rdi
    cmp r8, 0
    jge .abs

    neg r8
    mov rdi, [numbers+10]
    call print_char

    .abs:
        mov rdi, r8
        call print_uint
    xor rax, rax
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе rdi rsi
string_equals: 
    xor rcx, rcx
    xor rdx, rdx

.loop:
    mov cl, [rsi]
    mov dl, [rdi]

    cmp cl, dl
    jne .ERRR
    cmp cl, 0
    je .end_test

    inc rsi
    inc rdi
    jmp .loop

.end_test:
    mov rax, 1
    ret
.ERRR:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall


    xor rax, rax
    mov al, byte[rsp]
    pop r8
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor r8, r8
    push rdi ;buffer
    push rsi ;buffersize
.skiploop:
    call read_char

    cmp rax, 0x20
    je .skiploop

    cmp rax, 0xA
    je .skiploop

    cmp rax, 0x9
    je .skiploop

    pop rsi
    pop rdi

    cmp rax, 0x0
    je .err

    mov [rdi], al
    inc r8
.loop:
    push rdi
    push rsi
    call read_char
    pop rsi
    pop rdi
    mov [rdi+r8], al
    inc r8

    cmp al, 0x0
    je .end
    cmp al, 0x20
    je .end
    cmp al, 0xA
    je .end
    cmp al, 0x9
    je .end

    cmp r8, rsi
    jb .loop

.err:
    xor rax, rax
    xor rdx, rdx
    ret

.end:
    dec r8
    mov byte[rdi+r8], 0x0
    mov rax, rdi
    mov rdx, r8
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint: ;rdi
  xor rax, rax
  xor rcx, rcx
.A:
  mov cl, [rdi]
  inc rdi
  cmp cl, '0'
  jb .ERR
  cmp cl, '9'
  ja .ERR
  mov r8, 1
  sub cl, '0'
  mov rax, rcx 
  jmp .B
.B:
  mov cl, [rdi]
  inc rdi
  cmp cl, '0'
  jb .ok
  cmp cl, '9'
  ja .ok

  inc r8
  mov r10, 10
  mul r10  
  sub cl, '0'
  add rax, rcx

  jmp .B
.ok:
  mov rdx, r8
  ret   
.ERR:
  xor rdx, rdx
  ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:; rdi
    push r12
    xor r12, r12
    xor rcx, rcx
    mov cl, [rdi]
    cmp cl, '-'
    jne .u_or_not
    mov r12, 1
    inc rdi

.u_or_not:
    call parse_uint

    cmp rdx, 0
    je .ERR
    
    cmp r12, 1
    jne .ok

    inc rdx
    neg rax

.ok:
    pop r12
    ret
  
.ERR:
    pop r12
    xor rdx, rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
 ;rdi - str  rsi - buffer rdx - size
string_copy:     
    call string_length ;rax => str_len
    cmp rax, rdx
    jae .exit    

    xor rcx, rcx
    .loop:
        
        mov cl, [rdi]
        mov [rsi], cl
        sub rdx, 1
        inc rsi
        inc rdi
        test rdx, rdx
        jnz .loop

    ret
    
    .exit:
        mov rax, 0
        ret
